# Majframe module-admin

![Packagist Version](https://img.shields.io/packagist/v/majframe/module-admin?label=version)
![PHP from Packagist](https://img.shields.io/packagist/php-v/majframe/module-admin)
![Packagist](https://img.shields.io/packagist/l/majframe/module-admin)
![Libraries.io dependency status for GitHub repo](https://img.shields.io/librariesio/github/majframe/module-admin)
![GitHub issues](https://img.shields.io/github/issues/majframe/module-admin)
![GitHub closed issues](https://img.shields.io/github/issues-closed-raw/majframe/module-admin)
![GitHub repo size](https://img.shields.io/github/repo-size/majframe/module-admin)
![Packagist](https://img.shields.io/packagist/dt/majframe/module-admin)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Majframe framework
* PHP 7.4

### Installing

Recommended installation is using composer.

```
composer require majframe/module-admin
```

## Authors

* **Ondřej Maxa** - ondrej@maxa.expert - http://ondrej.maxa.expert/

## License

This project is licensed under the GPL v3.0 License - see the [LICENSE.md](LICENSE.md) file for details
